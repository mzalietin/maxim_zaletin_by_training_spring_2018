<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>Account</title>
    <style>
        <%@include file="css/common.css"%>
    </style>
</head>

<body>
    <c:import url="header.jsp"/>
    <div class="common">
        <h2>User profile</h2>
        <p>
            <b>Name</b> ${user.username}
        </p>
        <p>
            <b>Login</b> ${user.login}
        </p>
        <p>
            <b>Password</b> ${user.password}
        </p>
    </div>
</body>
</html>
