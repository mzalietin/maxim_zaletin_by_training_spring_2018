<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8"%>

<html>
<head>
    <title>Sign in</title>
    <style>
        <%@include file="css/common.css"%>
    </style>
</head>

<body>
    <c:import url="header.jsp"/>
    <div class="common">
        <div class="form">
            <form action="/auth" method="post">
                <table>
                    <tr>
                        <td><label for="Login">Login</label></td>
                        <td><input type="text" name="login" id="Login"></td>
                    </tr>

                    <tr>
                        <td><label for="Password">Password</label></td>
                        <td><input type="password" name="password" id="Password"></td>
                    </tr>
                </table>
                <br>
                <input type="submit" value="OK">
            </form>
            <font color="white">
                <c:if test="${status != null}">
                    ${status}
                </c:if>
            </font>
        </div>
    </div>
</body>
</html>
