<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>Main</title>
    <style>
        <%@include file="css/common.css"%>
    </style>
</head>

<body>
    <c:import url="header.jsp"/>
    <div class="common">
        This is welcome page. Expecting some content soon :)<br>
        <ul>
            <c:choose>
                <c:when test="${user == null}">
                    <li><a href="login.jsp">Login</a><br></li>
                </c:when>
            </c:choose>
            <li><a href="task.jsp">Training task</a></li>
        </ul>
    </div>
</body>
</html>
