<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>Title</title>
    <style>
        <%@include file="css/header.css"%>
    </style>
</head>

<body class="header">
    <div class="logo">
        <a href="/index.jsp">
            <b>Web project sample</b>
        </a>
    </div>

    <div class="login-bar">
        <c:choose>
            <c:when test="${user != null}">
                ${user.login} <a href="out">Logout</a> <a href="account.jsp">Account</a>
            </c:when>
            <c:otherwise>
                Guest
            </c:otherwise>
        </c:choose>
    </div>
</body>
</html>
