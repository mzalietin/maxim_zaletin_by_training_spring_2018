<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="utils" uri="web-utility.tags" %>

<html>
<head>
    <title>Task</title>
</head>

<body>
    <h2>Custom tags usage</h2>
    <h3>listmapping</h3>
    <p>
        <utils:listmapping>
            {url} - {servlet}
        </utils:listmapping>
    </p>
    <h3>resolveurl</h3>
    <p>
        <utils:resolveurl url="/auth"/>
    </p>
</body>
</html>
