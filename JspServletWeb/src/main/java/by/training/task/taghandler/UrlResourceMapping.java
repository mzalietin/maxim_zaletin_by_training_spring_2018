package by.training.task.taghandler;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class UrlResourceMapping extends SimpleTagSupport {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void doTag() throws JspException, IOException {
        if (url != null) {
            PageContext pc = (PageContext) getJspContext();
            JspWriter output = pc.getOut();
            final String separator = " - ";

            output.println(url + separator + pc.getServletContext().getRealPath(url));
        }
    }
}
