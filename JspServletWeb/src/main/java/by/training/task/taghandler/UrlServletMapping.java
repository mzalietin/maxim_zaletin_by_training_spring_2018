package by.training.task.taghandler;

import javax.servlet.ServletRegistration;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.io.IOException;
import java.util.Map;

public class UrlServletMapping extends BodyTagSupport {

    @Override
    public int doStartTag() throws JspException {
        return EVAL_BODY_INCLUDE;
    }

    @Override
    public int doAfterBody() throws JspException {
        final String separator = " - ";
        try {
            JspWriter output = pageContext.getOut();
            final String htmlLn = "<br>";
            for (Map.Entry<String, ? extends ServletRegistration> entry :
                    pageContext.getServletContext().getServletRegistrations().entrySet()) {
                output.print(htmlLn);
                output.print(entry.getValue().getMappings() + separator + entry.getKey());
            }
        } catch (IOException e) {
            pageContext.getServletContext().log("Error writing to page output", e);
        }

        return SKIP_BODY;
    }
}
