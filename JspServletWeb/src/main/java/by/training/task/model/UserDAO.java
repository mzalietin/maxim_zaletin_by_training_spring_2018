package by.training.task.model;

import java.util.HashMap;
import java.util.Map;

public class UserDAO {
    private static final Map<User, String> storage = new HashMap<>();

    static {
        storage.put(new User("lappid.mail@gmail.com", "qwerty123"), "Maxim 'lappid' Zaletin");
        storage.put(new User("andrew_p133@mail.ru", "cool888android"), "Andrew 'andrew_p133' Prokhorenko");
    }

    public static boolean validate(User user) {
        if (storage.containsKey(user)) {
            user.setUsername(storage.get(user));
            return true;
        } else {
            return false;
        }
    }
}
