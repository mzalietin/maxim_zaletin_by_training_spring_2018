package by.training.task.controller.impl;

import by.training.task.model.User;
import by.training.task.controller.AbstractController;
import by.training.task.model.UserDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginController extends AbstractController {
    @Override
    protected void handle(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login").trim();
        String password = req.getParameter("password").trim();
        User someUser = new User(login, password);
        if (UserDAO.validate(someUser)) {
            req.getSession().setAttribute("user", someUser);
            jumpPage(req, resp, "account.jsp");
        } else {
            jumpError("No such user", req, resp, "login.jsp");
        }
    }
}
