package by.training.task.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class AbstractController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handle(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handle(req, resp);
    }

    protected abstract void handle(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException;

    protected void jumpPage(HttpServletRequest request, HttpServletResponse response, String forwardPage)
            throws ServletException, IOException {
        request.getRequestDispatcher(forwardPage).forward(request, response);
    }

    protected void jumpError(String message, HttpServletRequest request, HttpServletResponse response, String forwardPage)
            throws ServletException, IOException {
        request.setAttribute("status", message);
        jumpPage(request, response, forwardPage);
    }
}
