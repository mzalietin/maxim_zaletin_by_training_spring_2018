package by.training.tasks.cluster.node.schema;

import by.training.tasks.cluster.node.entity.JsonEntity;
import by.training.tasks.cluster.node.exception.JsonNullException;
import by.training.tasks.cluster.node.exception.SchemaBuildException;
import by.training.tasks.cluster.node.exception.SchemaValidationException;
import by.training.tasks.cluster.node.json.PresentationParser;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.HashMap;
import java.util.Map;

public class JsonSchema implements DataSchema<JsonEntity> {

    private enum Type {
        STRING {
            @Override
            void checkString(String strValue) throws SchemaValidationException {
                if (strValue.isEmpty()) {
                    throw new SchemaValidationException("Empty string value found");
                }
            }
        },
        INTEGER {
            @Override
            void checkString(String strValue) throws SchemaValidationException {
                try {
                    Integer.parseInt(strValue);
                } catch (NumberFormatException e) {
                    throw new SchemaValidationException("Wrong integer format: " + strValue);
                }
            }
        },
        NUMBER {
            @Override
            void checkString(String strValue) throws SchemaValidationException {
                if (!NumberUtils.isCreatable(strValue)) {
                    throw new SchemaValidationException("Wrong number format: " + strValue);
                }
            }
        };

        abstract void checkString(String strValue) throws SchemaValidationException;
    }

    private final Map<String, Type> schema;

    public JsonSchema(String jsonString) throws SchemaBuildException {
        Map<String, String> presentation;
        try {
            presentation = PresentationParser.jsonToMap(jsonString);
        } catch (JsonNullException e) {
            throw new SchemaBuildException(e);
        }

        //schema build
        schema = new HashMap<>();
        for (Map.Entry<String, String> entry: presentation.entrySet()) {
            Type type;
            try {
                type = Type.valueOf(entry.getValue().toUpperCase());
            } catch (IllegalArgumentException e) {
                throw new SchemaBuildException(entry.getKey(), entry.getValue());
            }
            schema.putIfAbsent(entry.getKey(), type);
        }
    }

    @Override
    public void validate(JsonEntity entity) throws SchemaValidationException {
        for (Map.Entry<String, String> field: entity.getPresentation().entrySet()) {
            if (schema.containsKey(field.getKey())) {
                Type possibleType = schema.get(field.getKey());
                possibleType.checkString(field.getValue());
            } else {
                throw new SchemaValidationException("Field \"" + field.getKey() + "\" not found in defined JSON schema");
            }
        }
    }

    @Override
    public String toString() {
        Map<String, String> presMap = new HashMap<>();
        schema.forEach((k, v) -> presMap.put(k, v.toString().toLowerCase()));
        return PresentationParser.mapToJson(presMap);
    }
}
