package by.training.tasks.cluster.servlet.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class ParametersFilter implements Filter {
    private static final int COLLECTION_NAME_URI_INDEX = 3;
    public static final String COLLECTION_NAME_ATTRIBUTE = "collection";

    private static final int ENTITY_ID_URI_INDEX = 4;
    public static final String ENTITY_ID_ATTRIBUTE = "id";

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest httpReq = (HttpServletRequest) servletRequest;
        String[] uriTokens = httpReq.getRequestURI().split("/");

        String collectionName;
        try {
            collectionName = uriTokens[COLLECTION_NAME_URI_INDEX];
        } catch (ArrayIndexOutOfBoundsException e) {
            collectionName = "";
        }
        httpReq.setAttribute(COLLECTION_NAME_ATTRIBUTE, collectionName);

        String entityId;
        try {
            entityId = uriTokens[ENTITY_ID_URI_INDEX];
        } catch (ArrayIndexOutOfBoundsException e) {
            entityId = "";
        }
        httpReq.setAttribute(ENTITY_ID_ATTRIBUTE, entityId);

        filterChain.doFilter(httpReq, servletResponse);
    }

    //---java.lang.AbstractMethodError

    @Override
    public void init(FilterConfig filterConfig) throws ServletException { }

    @Override
    public void destroy() { }
}
