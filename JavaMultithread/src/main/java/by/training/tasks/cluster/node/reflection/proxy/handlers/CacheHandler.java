package by.training.tasks.cluster.node.reflection.proxy.handlers;

import by.training.tasks.cluster.node.cache.Cache;
import by.training.tasks.cluster.node.entity.JsonEntity;
import by.training.tasks.cluster.node.exception.SchemaValidationException;
import com.google.gson.JsonElement;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class CacheHandler implements InvocationHandler {
    private final Cache<Integer, JsonEntity> cache;
    private Object daoCore;

    public CacheHandler(Cache<Integer, JsonEntity> cache) {
        this.cache = cache;
    }

    public void setTarget(Object target) {
        daoCore = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String methodName = method.getName();

        switch (methodName) {
            case "create": {
                try {
                    JsonEntity entity = (JsonEntity) args[0];

                    int id = (int) method.invoke(daoCore, entity);
                    cache.insertIfAbsent(id, entity);
                    return id;
                } catch (InvocationTargetException e) {
                    throw e.getCause();
                }
            }

            case "getOne": {
                int id = (int) args[0];
                boolean cached = (boolean) args[1];

                if (cached) {
                    return cache.retrieve(id);
                } else {
                    return null;
                }
            }

            case "update": {
                try {
                    JsonEntity entity = (JsonEntity) args[0];

                    cache.updateIfPresent(entity.getId(), entity);
                    method.invoke(daoCore, entity);
                    return Void.TYPE;
                } catch (InvocationTargetException e) {
                    throw e.getCause();
                }
            }

            case "delete": {
                int id = (int) args[0];

                cache.delete(id);
                method.invoke(daoCore, id);
                return Void.TYPE;
            }

            default: {
                return method.invoke(daoCore, args);
            }
        }
    }
}
