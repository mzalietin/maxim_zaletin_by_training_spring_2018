package by.training.tasks.cluster.servlet;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.*;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.*;
import java.util.*;
import java.util.logging.Level;

public class ClusterScope implements ResourceLoaderAware {
    private Map<String, Map<String, String>> cluster;

    private HealthCheckService healthCheckService;

    private final String confFile;
    private final String thisNode;
    private final String thisShard;

    private static final String NODE_PREFIX = "node-";
    public static final String CLUSTER_REQUEST_HEADER = "cluster-host";

    private final CloseableHttpClient httpClient = HttpClients.createDefault();

    public ClusterScope(String shardName, String clusterConfFile) {
        confFile = clusterConfFile;

        final String SEPARATOR = "\\.";
        String[] tokens = shardName.split(SEPARATOR);

        thisNode = tokens[0];
        thisShard = tokens[1];
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        try {
            final String CP_PREFIX = "classpath:";
            Resource clusterConf = resourceLoader.getResource(CP_PREFIX + confFile) ;
            BufferedReader reader = new BufferedReader(new InputStreamReader(clusterConf.getInputStream()));
            cluster = new Gson().fromJson(reader, new TypeToken<Map<String, Map<String, String>>>() {}.getType());

            Map<String, ShardStatus> healthStatus = new HashMap<>();
            cluster.forEach((nodeName, shard) ->
                    shard.forEach((shardName, Host) -> healthStatus.putIfAbsent(Host, ShardStatus.GREEN)));

            healthCheckService = new HealthCheckService(healthStatus);
        } catch (IOException e) {
            java.util.logging.LogManager.getLogManager()
                    .getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
        }
    }

    public String getProperAddress(String input) {
        int nodesCount = cluster.size();
        int properNodeNum = input.hashCode() % nodesCount;

        int currNode = Integer.parseInt(thisNode.replace(NODE_PREFIX, ""));

        if (currNode == properNodeNum) {
            return cluster.get(thisNode).get(thisShard);
        } else {
            String properNode = NODE_PREFIX + properNodeNum;

            for (Map.Entry<String, String> entry: cluster.get(properNode).entrySet()) {
                String shardHost = entry.getValue();
                if (healthCheckService.refreshStatus(shardHost) == ShardStatus.GREEN) {
                    return shardHost;
                }
            }

            return null;
        }
    }

    public boolean isAnotherNodeOrExternalAddress(String address) {
        return address == null || !cluster.get(thisNode).containsValue(address);
    }

    public HealthCheckService getHealthCheckService() {
        return healthCheckService;
    }

    public class Response {
        private int statusCode;
        private String body;

        Response(HttpResponse response) throws IOException {
            statusCode = response.getStatusLine().getStatusCode();

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                body = EntityUtils.toString(entity, ContentType.APPLICATION_JSON.getCharset());
            }
        }

        public int getStatusCode() {
            return statusCode;
        }

        public String getBody() {
            return body;
        }
    }

    public Response redirectRequest(String host, String uri, String method, String body) {
        HttpUriRequest request = null;

        if (method.equals(HttpGet.METHOD_NAME)) {
            request = new HttpGet(host + uri);
        }
        if (method.equals(HttpDelete.METHOD_NAME)) {
            request = new HttpDelete(host + uri);
        }

        StringEntity entity = new StringEntity(body, ContentType.APPLICATION_JSON);
        if (method.equals(HttpPost.METHOD_NAME)) {
            HttpPost post = new HttpPost(host + uri);
            post.setEntity(entity);
            request = post;
        }
        if (method.equals(HttpPut.METHOD_NAME)) {
            HttpPut put = new HttpPut(host + uri);
            put.setEntity(entity);
            request = put;
        }

        request.addHeader(CLUSTER_REQUEST_HEADER, cluster.get(thisNode).get(thisShard));
        try {
            try (CloseableHttpResponse response = httpClient.execute(request)) {
                return new Response(response);
            }
        } catch (IOException e) {
            healthCheckService.setStatus(host, ShardStatus.RED);
            return null;
        }
    }

    public void replicateOnPost(String uri, String content) {
        cluster.get(thisNode).forEach((shardName, shardHost) -> {
            if (!shardName.equals(thisShard)) {
                if (healthCheckService.refreshStatus(shardHost) == ShardStatus.GREEN) {
                    HttpPost post = new HttpPost(shardHost + uri);
                    post.addHeader(CLUSTER_REQUEST_HEADER, cluster.get(thisNode).get(thisShard));
                    StringEntity entity = new StringEntity(content, ContentType.APPLICATION_JSON);
                    post.setEntity(entity);
                    try {
                        httpClient.execute(post);
                    } catch (IOException e) {
                        healthCheckService.setStatus(shardHost, ShardStatus.RED);
                    }
                }
            }
        });
    }

    private String searchEntity(String host, String uri) {
        String result = null;

        HttpGet get = new HttpGet(host + uri);
        get.addHeader(CLUSTER_REQUEST_HEADER, cluster.get(thisNode).get(thisShard));
        try {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    HttpEntity entity = response.getEntity();
                    result = EntityUtils.toString(entity, ContentType.APPLICATION_JSON.getCharset());
                }
            }
        } catch (IOException e) {
            healthCheckService.setStatus(host, ShardStatus.RED);
        }

        return result;
    }

    public List<String> searchAllCollectionsOnGet(String uri) {
        List<String> allCollections = new LinkedList<>();
        for (Map.Entry<String, Map<String, String>> nodeEntry: cluster.entrySet()) {
            if (!nodeEntry.getKey().equals(thisNode)) {
                for (Map.Entry<String, String> shardEntry : nodeEntry.getValue().entrySet()) {
                    String shardHost = shardEntry.getValue();

                    if (healthCheckService.refreshStatus(shardHost) == ShardStatus.GREEN) {
                        String jsonContent = searchEntity(shardHost, uri);
                        if (jsonContent != null) {
                            List<String> shardCollections = new Gson().fromJson(jsonContent,
                                    new TypeToken<List<String>>() {
                                    }.getType());

                            allCollections.addAll(shardCollections);

                            break;
                        }
                    }
                }
            }
        }

        return allCollections;
    }

    public String searchObjectOnGet(String uri) {
        String result = null;

        for (Map.Entry<String, String> entry: cluster.get(thisNode).entrySet()) {
            if (!entry.getKey().equals(thisShard)) {
                String shardHost = entry.getValue();

                if (healthCheckService.refreshStatus(shardHost) == ShardStatus.GREEN) {
                    result = searchEntity(shardHost, uri);
                    if (result != null) {
                        break;
                    }
                }
            }
        }

        return result;
    }

    public void synchronizeOnUpdate(String uri, String content) {
        cluster.get(thisNode).forEach((shardName, shardHost) -> {
            if (!shardName.equals(thisShard)) {
                if (healthCheckService.refreshStatus(shardHost) == ShardStatus.GREEN) {
                    HttpPut put = new HttpPut(shardHost + uri);
                    put.addHeader(CLUSTER_REQUEST_HEADER, cluster.get(thisNode).get(thisShard));
                    StringEntity entity = new StringEntity(content, ContentType.APPLICATION_JSON);
                    put.setEntity(entity);
                    try {
                        httpClient.execute(put);
                    } catch (IOException e) {
                        healthCheckService.setStatus(shardHost, ShardStatus.RED);
                    }
                }
            }
        });
    }

    public void synchronizeOnDelete(String uri) {
        cluster.get(thisNode).forEach((shardName, shardHost) -> {
            if (!shardName.equals(thisShard)) {
                if (healthCheckService.refreshStatus(shardHost) == ShardStatus.GREEN) {
                    HttpDelete delete = new HttpDelete(shardHost + uri);
                    delete.addHeader(CLUSTER_REQUEST_HEADER, cluster.get(thisNode).get(thisShard));
                    try {
                        httpClient.execute(delete);
                    } catch (IOException e) {
                        healthCheckService.setStatus(shardHost, ShardStatus.RED);
                    }
                }
            }
        });
    }
}
