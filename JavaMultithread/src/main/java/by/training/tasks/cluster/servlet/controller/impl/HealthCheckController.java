package by.training.tasks.cluster.servlet.controller.impl;

import by.training.tasks.cluster.servlet.ClusterScope;
import by.training.tasks.cluster.servlet.HealthCheckService;
import by.training.tasks.cluster.servlet.controller.ClusterController;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HealthCheckController extends ClusterController {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ClusterScope clusterScope = getClusterScope();
        HealthCheckService healthCheckService = clusterScope.getHealthCheckService();

        String json = new Gson().toJson(healthCheckService.getClusterStatus());
        sendJsonResponse(resp, json);
    }
}
