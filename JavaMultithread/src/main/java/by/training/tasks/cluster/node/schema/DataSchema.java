package by.training.tasks.cluster.node.schema;

import by.training.tasks.cluster.node.exception.SchemaValidationException;

public interface DataSchema<T> {
    void validate(T entity) throws SchemaValidationException;
}
