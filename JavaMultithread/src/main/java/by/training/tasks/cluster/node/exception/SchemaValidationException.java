package by.training.tasks.cluster.node.exception;

public class SchemaValidationException extends Exception {
    public SchemaValidationException() { }

    public SchemaValidationException(String message) {
        super(message);
    }

    public SchemaValidationException(Throwable cause) {
        super(cause);
    }
}
