package by.training.tasks.cluster.node.dao;

import by.training.tasks.cluster.node.entity.JsonEntity;
import by.training.tasks.cluster.node.exception.SchemaValidationException;
import by.training.tasks.cluster.node.reflection.annotation.Proxy;
import by.training.tasks.cluster.node.schema.DataSchema;
import by.training.tasks.cluster.node.schema.JsonSchema;

import static by.training.tasks.cluster.node.dao.SqlProvider.*;

import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SingleColumnRowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Proxy
public class JsonDao implements NodeDao<JsonEntity> {
    private JsonSchema dataSchema;
    private JdbcTemplate jdbcTemplateObject;
    private SqlProvider sqlProvider;

    public JsonDao() { }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplateObject) {
        this.jdbcTemplateObject = jdbcTemplateObject;
    }

    public void setDataSchema(JsonSchema dataSchema) {
        this.dataSchema = dataSchema;
    }

    public void setSqlProvider(SqlProvider sqlProvider) {
        this.sqlProvider = sqlProvider;
    }

    @Override
    public void init() {
        jdbcTemplateObject.execute(sqlProvider.getQuery(SQL_CREATE_SEQUENCE));
        jdbcTemplateObject.execute(sqlProvider.getQuery(SQL_CREATE_TABLE));
    }

    @Override
    public int create(JsonEntity entity) throws SchemaValidationException {
        dataSchema.validate(entity);
        int id = jdbcTemplateObject.queryForObject(sqlProvider.getQuery(SQL_INSERT_OBJECT),
                new Object[] {entity.toString()}, int.class);

        entity.setId(id);
        return id;
    }

    @Override
    public JsonEntity getOne(int id, boolean cached) {
        if (!cached) {
            try {
                String jsonData = jdbcTemplateObject.queryForObject(
                        sqlProvider.getQuery(SQL_SELECT_OBJECT_BY_ID),
                        new Object[]{id},
                        String.class);

                return new JsonEntity(id, jsonData);
            } catch (IncorrectResultSizeDataAccessException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public List<JsonEntity> getAll() {
        return jdbcTemplateObject.query(sqlProvider.getQuery(SQL_SELECT_ALL),
                new SingleColumnRowMapper<JsonEntity>() {
                    @Override
                    public JsonEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return new JsonEntity(rs.getInt(1), rs.getString(2));
                    }
                }
        );
    }

    @Override
    public void update(JsonEntity entity) throws SchemaValidationException {
        dataSchema.validate(entity);

        jdbcTemplateObject.update(
                sqlProvider.getQuery(SQL_UPDATE_OBJECT),
                entity.toString(), entity.getId());
    }

    @Override
    public void delete(int id) {
        jdbcTemplateObject.update(sqlProvider.getQuery(SQL_DELETE_OBJECT_BY_ID), id);
    }

    @Override
    public DataSchema<JsonEntity> getDataSchema() {
        return dataSchema;
    }

    @Override
    public void shutDown() {
        jdbcTemplateObject.execute(sqlProvider.getQuery(SQL_DROP_TABLE));
    }
}
