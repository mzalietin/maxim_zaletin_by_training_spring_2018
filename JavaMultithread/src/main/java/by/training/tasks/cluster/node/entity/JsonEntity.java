package by.training.tasks.cluster.node.entity;

import by.training.tasks.cluster.node.json.PresentationParser;

import java.util.Map;
import java.util.Objects;

public class JsonEntity implements NodeEntity<Map<String, String>> {
    private int id;
    private Map<String, String> presentation;

    public JsonEntity(int id, String jsonString) {
        this.id = id;

        presentation = PresentationParser.jsonToMap(Objects.requireNonNull(jsonString));
    }

    public JsonEntity(String jsonString) {
        presentation = PresentationParser.jsonToMap(Objects.requireNonNull(jsonString));
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public Map<String, String> getPresentation() {
        return presentation;
    }

    @Override
    public String toString() {
        return PresentationParser.mapToJson(presentation);
    }
}
