package by.training.tasks.cluster.node;

import by.training.tasks.cluster.node.dao.NodeDao;

import java.util.List;

public interface INodeShard {
    boolean createCollection(String name, NodeDao collection);
    NodeDao getCollection(String name);
    void removeCollection(String name);
    boolean collectionExists(String name);
    void updateCollection(String name, NodeDao collection);
    List<String> getCollectionsList();
}
