package by.training.tasks.cluster.node.exception;

public class SchemaBuildException extends Exception {
    private static final String MESSAGE_START = "Cannot resolve JSON type for field \"";
    private static final String MESSAGE_INNER = "\" with type \"";
    private static final String MESSAGE_END = "\"";

    public SchemaBuildException() { }

    public SchemaBuildException(String message) {
        super(message);
    }

    public SchemaBuildException(String fieldName, String fieldType) {
        super(MESSAGE_START + fieldName + MESSAGE_INNER + fieldType + MESSAGE_END);
    }

    public SchemaBuildException(Throwable cause) {
        super(cause);
    }
}
