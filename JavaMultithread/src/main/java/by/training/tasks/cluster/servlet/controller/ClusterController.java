package by.training.tasks.cluster.servlet.controller;

import by.training.tasks.cluster.node.NodeShard;
import by.training.tasks.cluster.servlet.ClusterScope;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

public abstract class ClusterController extends WebApplicationController {

    protected NodeShard getNode() {
        return getAppContext().getBean(NodeShard.class);
    }

    protected ClusterScope getClusterScope() {
        return getAppContext().getBean(ClusterScope.class);
    }

    protected void sendJsonResponse(HttpServletResponse resp, String json) throws IOException {
        final String JSON_MIME = "application/json", UTF_8 = "UTF-8";

        resp.setContentType(JSON_MIME);
        resp.setCharacterEncoding(UTF_8);
        sendResponseWithBody(resp, json);
    }

    protected void sendResponseWithBody(HttpServletResponse resp, String body) throws IOException {
        resp.getWriter().write(body);
        resp.getWriter().flush();
    }

    protected void sendResponseWhenCollectionNotFound(HttpServletRequest req, HttpServletResponse resp,
                                                      String collectionName) throws IOException {
        ClusterScope cluster = getClusterScope();
        String targetAddress = cluster.getProperAddress(collectionName);

        if (targetAddress == null) {
            resp.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, ServerMessages.NODE_IS_DOWN);
        } else {
            if (req.getRequestURL().toString().contains(targetAddress)) {
                //this node is proper but not found here
                resp.sendError(HttpServletResponse.SC_NOT_FOUND, ServerMessages.COLLECTION_NOT_FOUND);
            } else {
                String body = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

                ClusterScope.Response response = cluster
                        .redirectRequest(targetAddress, req.getRequestURI(), req.getMethod(), body);

                int respSc = response.getStatusCode();
                String respBody = response.getBody();

                resp.setStatus(respSc);

                if (respSc >= HttpServletResponse.SC_OK && respSc < HttpServletResponse.SC_MULTIPLE_CHOICES) {
                    sendJsonResponse(resp, respBody);
                } else {
                    sendResponseWithBody(resp, respBody);
                }
            }
        }
    }
}
