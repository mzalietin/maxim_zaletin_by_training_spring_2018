package by.training.tasks.cluster.node.reflection.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationHandler;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)

public @interface Proxy {
    Class invocationHandler() default InvocationHandler.class;
}
