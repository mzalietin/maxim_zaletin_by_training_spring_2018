package by.training.tasks.cluster.node.cache;

/**
 * Interface declares basic methods for map-kind data cache, <code>null</code> as key/value restricted.
 *
 * @param <K> key type
 * @param <V> value type
 */
public interface Cache<K, V> {

    /**
     * Method to add data in cache.
     *
     * @param key key to store.
     * @param value value to store.
     */
    void insertIfAbsent(K key, V value);

    /**
     * Method to get data entry from storage. If no entry with such key exist or argument is <code>null</code>,
     * returns <code>null</code>.
     *
     * @param key key of data entry.
     * @return entry value.
     */
    V retrieve(K key);

    /**
     * Associates new value with given key only if that key exists in cache.
     * @param key entry key
     * @param value new value
     */
    void updateIfPresent(K key, V value);

    /**
     * Removes data entry from cache.
     *
     * @param key entry key.
     */
    void delete(K key);

    /**
     * Removes all data entries.
     */
    void flush();
}
