package by.training.tasks.cluster.node;

import by.training.tasks.cluster.node.dao.NodeDao;

import java.util.*;

public class NodeShard implements INodeShard {
    private final Map<String, NodeDao> collections;
    private final int capacity;

    public NodeShard(int capacity) {
        if (capacity <= 0) {
            throw new IllegalArgumentException("Capacity must be above zero");
        }
        this.capacity = capacity;
        collections = new HashMap<>(capacity, 1.0f);
    }

    @Override
    public boolean createCollection(String name, NodeDao dao) {
        Objects.requireNonNull(name);
        Objects.requireNonNull(dao);

        boolean result = false;

        if (collections.size() < capacity) {
            dao.init();
            collections.put(name, dao);
            result = true;
        }

        return result;
    }

    @Override
    public NodeDao getCollection(String name) {
        Objects.requireNonNull(name);

        return collections.get(name);
    }

    @Override
    public boolean collectionExists(String name) {
        return collections.containsKey(name);
    }

    @Override
    public List<String> getCollectionsList() {
        return new ArrayList<>(collections.keySet());
    }

    @Override
    public void updateCollection(String name, NodeDao dao) {
        Objects.requireNonNull(name);
        Objects.requireNonNull(dao);

        collections.remove(name).shutDown();

        dao.init();
        collections.put(name, dao);
    }

    @Override
    public void removeCollection(String name) {
        Objects.requireNonNull(name);

        collections.remove(name).shutDown();
    }
}
