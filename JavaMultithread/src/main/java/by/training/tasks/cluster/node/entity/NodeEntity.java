package by.training.tasks.cluster.node.entity;

public interface NodeEntity<P> {
    int getId();
    void setId(int id);
    P getPresentation();
}
