package by.training.tasks.cluster.servlet;

public enum ShardStatus {
    GREEN, RED
}
