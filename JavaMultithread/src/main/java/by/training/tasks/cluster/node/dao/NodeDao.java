package by.training.tasks.cluster.node.dao;

import by.training.tasks.cluster.node.exception.SchemaValidationException;
import by.training.tasks.cluster.node.schema.DataSchema;

import java.util.List;

public interface NodeDao<T> {
    void init();
    int create(T object) throws SchemaValidationException;
    T getOne(int id, boolean cached);
    List<T> getAll();
    void update(T object) throws SchemaValidationException;
    void delete(int id);
    DataSchema<T> getDataSchema();
    void shutDown();
}
