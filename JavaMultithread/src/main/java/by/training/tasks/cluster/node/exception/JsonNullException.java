package by.training.tasks.cluster.node.exception;

public class JsonNullException extends RuntimeException {
    public JsonNullException() {}

    public JsonNullException(String message) {
        super(message);
    }
}
