package by.training.tasks.cluster.node.cache;

public enum CacheFactory {
    LFU {
        @Override
        Cache getImpl(int size) {
            return new LeastFrequentlyUsedCache(size);
        }
    },
    LRU {
        @Override
        Cache getImpl(int size) {
            return new LeastRecentlyUsedCache(size);
        }
    };

    abstract Cache getImpl(int size);

    public static Cache getCache(String algorithm, int size) {
        return valueOf(algorithm.toUpperCase()).getImpl(size);
    }
}
