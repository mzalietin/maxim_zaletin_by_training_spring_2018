package by.training.tasks.cluster.node.cache;

import java.util.*;

public class LeastRecentlyUsedCache<K, V> implements Cache<K, V> {
    private final int capacity;
    private final Map<K, V> storage;

    public LeastRecentlyUsedCache(int maxSize) {
        if (maxSize > 0) {
            capacity = maxSize;
            storage = Collections.synchronizedMap(new LinkedHashMap<K, V>(maxSize + 1, 1.0f, true) {
                @Override
                protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
                    return this.size() > capacity;
                }
            });
        } else {
            throw new IllegalArgumentException("Cache size must be greater than zero");
        }
    }

    @Override
    public void insertIfAbsent(K key, V value) {
        Objects.requireNonNull(key);
        Objects.requireNonNull(value);

        storage.putIfAbsent(key, value);
    }

    @Override
    public V retrieve(K key) {
        return storage.get(key);
    }

    @Override
    public void updateIfPresent(K key, V value) {
        Objects.requireNonNull(key);
        Objects.requireNonNull(value);

        if (storage.containsKey(key)) {
            storage.put(key, value);
        }
    }

    @Override
    public void delete(K key) {
        storage.remove(key);
    }

    @Override
    public void flush() {
        storage.clear();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        final String keyValueSeparator = " : ";
        sb.append("LRU").append(System.lineSeparator());
        for (Map.Entry<K, V> entry: storage.entrySet()) {
            sb.append(entry.getKey()).append(keyValueSeparator).append(entry.getValue()).append(System.lineSeparator());
        }
        return sb.toString();
    }
}
