package by.training.tasks.cluster.node.reflection.serialization;

import by.training.tasks.cluster.node.reflection.annotation.Serializable;
import org.apache.commons.lang3.math.NumberUtils;

import java.lang.reflect.Field;
import java.util.*;

/**
 * Serializes object's to JSON format, represented by map of strings.
 * Input object's class should have @Serializable annotation, else
 * the IllegalArgumentException will be thrown. Also, if incorrect
 * format of Map given to deserialize method, same exception will be
 * thrown.
 */
public class MapSerializer implements Serializer<Map<String, String>> {

    private static final List<Class> SERIALIZING_TYPES = Arrays.asList(new Class[] {
            int.class, float.class, long.class,
            byte.class, double.class, short.class,
            Byte.class, Double.class, Float.class,
            Integer.class, Long.class, Short.class,
            String.class
    });

    /**
     * Serializes only numeric and String field types, which have @Serializable
     * annotation specified and non-null value, else pass to next field.
     *
     * @param packingObj an object to serialize to json format
     * @return map presentation
     */
    public Map<String, String> serialize(Object packingObj) {
        Class objectClass = packingObj.getClass();

        if (objectClass.isAnnotationPresent(Serializable.class)) {

            Map<String, String> store = new HashMap<>();

            Field[] objectFields = objectClass.getDeclaredFields();
            for (Field f: objectFields) {
                f.setAccessible(true);
                if (f.isAnnotationPresent(Serializable.class)) {
                    if (SERIALIZING_TYPES.contains(f.getType())) {
                        String fieldKey = f.getName();
                        try {
                            Object fieldValue = f.get(packingObj);
                            if (fieldValue != null) {
                                store.put(fieldKey, fieldValue.toString());
                            }
                        } catch (IllegalAccessException e) {
                            throw new IllegalStateException("Cannot access to given object's field", e);
                        }
                    }
                }
            }
            return store;
        } else {
            throw new IllegalArgumentException("Class doesn't support custom serialization: " + objectClass);
        }
    }

    /**
     * Checks given class for serialization ability and creates its instance.
     * Then fills the fields with extracted from store values, matching the type of value to
     * field type with utility method.
     *
     * @param store map presentation
     * @return object restored from presentation
     */
    public <T> T deserialize(Map<String, String> store, Class<T> restoredClass) {
        T restoredObj;

        if (restoredClass.isAnnotationPresent(Serializable.class)) {
                try {
                    restoredObj = restoredClass.newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
                    throw new IllegalStateException("Serialized object's class is unreachable", e);
                }
        } else {
            throw new IllegalArgumentException("Class doesn't support custom serialization: " + restoredClass);
        }

        Field[] fields = restoredClass.getDeclaredFields();

        for (Field f: fields) {
            f.setAccessible(true);
            if (f.isAnnotationPresent(Serializable.class)) {
                if (SERIALIZING_TYPES.contains(f.getType())) {
                    String fieldKey = f.getName();
                    String stringValue = store.get(fieldKey);
                    Number numValue;
                    try {
                        if (NumberUtils.isCreatable(stringValue)) {
                            numValue = NumberUtils.createNumber(stringValue);
                            setNumberValue(f, restoredObj, numValue);
                        } else {
                            f.set(restoredObj, stringValue);
                        }
                    } catch (IllegalAccessException e) {
                        throw new IllegalStateException("Cannot access class field", e);
                    }
                }
            }
        }

        return restoredObj;
    }

    /**
     * Utility method to define actual type of numeric field.
     *
     * @param f field to set
     * @param restoredObj object which field should be set
     * @param numValue abstract number
     */
    private void setNumberValue(Field f, Object restoredObj, Number numValue) throws IllegalAccessException {
        Class fieldType = f.getType();
        if (fieldType.equals(int.class) || fieldType.equals(Integer.class)) {
            f.set(restoredObj, numValue.intValue());
        }
        if (fieldType.equals(byte.class) || fieldType.equals(Byte.class)) {
            f.set(restoredObj, numValue.byteValue());
        }
        if (fieldType.equals(double.class) || fieldType.equals(Double.class)) {
            f.set(restoredObj, numValue.doubleValue());
        }
        if (fieldType.equals(float.class) || fieldType.equals(Float.class)) {
            f.set(restoredObj, numValue.floatValue());
        }
        if (fieldType.equals(long.class) || fieldType.equals(Long.class)) {
            f.set(restoredObj, numValue.longValue());
        }
        if (fieldType.equals(short.class) || fieldType.equals(Short.class)) {
            f.set(restoredObj, numValue.shortValue());
        }
    }
}
