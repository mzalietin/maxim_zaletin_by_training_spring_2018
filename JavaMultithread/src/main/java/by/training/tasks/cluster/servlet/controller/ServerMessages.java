package by.training.tasks.cluster.servlet.controller;

public interface ServerMessages {
    String EMPTY_BODY = "Empty request body";

    String NODE_CAPACITY_OVERFLOW = "Cannot create new collection due to capacity overflow";
    String NODE_IS_DOWN = "Sorry, service is temporary unavailable";

    String COLLECTION_NOT_FOUND = "Requested collection not found";
    String COLLECTION_NAME_IS_EMPTY = "Collection name can not be empty";
    String COLLECTION_EXISTS = "This collection name is already engaged";

    String INVALID_ID_FORMAT = "Object's ID is incorrect value";
    String ENTITY_NOT_FOUND = "Requested object doesn't exist";
}
