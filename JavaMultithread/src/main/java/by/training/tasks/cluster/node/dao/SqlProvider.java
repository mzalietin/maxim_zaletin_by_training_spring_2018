package by.training.tasks.cluster.node.dao;

import java.util.ResourceBundle;

public class SqlProvider {

    private ResourceBundle queries;
    private String tableName;

    static final String SQL_CREATE_SEQUENCE = "create.seq";
    static final String SQL_CREATE_TABLE = "create.table";
    static final String SQL_INSERT_OBJECT = "insert.object";
    static final String SQL_SELECT_OBJECT_BY_ID = "select.data";
    static final String SQL_SELECT_ALL = "select.alldata";
    static final String SQL_UPDATE_OBJECT = "update.object";
    static final String SQL_DELETE_OBJECT_BY_ID = "delete.object";
    static final String SQL_DROP_TABLE = "delete.table";

    private static final String tableNameParam = "{collection}";

    public SqlProvider(String bundlePath) {
        queries = ResourceBundle.getBundle(bundlePath);
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    String getQuery(String key) {
        return queries.getString(key).replace(tableNameParam, tableName);
    }
}
