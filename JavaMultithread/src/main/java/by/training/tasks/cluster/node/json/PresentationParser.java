package by.training.tasks.cluster.node.json;

import by.training.tasks.cluster.node.exception.JsonNullException;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.HashMap;
import java.util.Map;

public class PresentationParser {

    public static Map<String, String> jsonToMap(String jsonString) throws JsonNullException {
        JsonObject schemaObject = new JsonParser().parse(jsonString).getAsJsonObject();
        checkNoJsonNull(schemaObject);

        Map<String, String> presentation = new HashMap<>();
        schemaObject.entrySet().forEach((entry) -> presentation.put(entry.getKey(), entry.getValue().getAsString()));

        return presentation;
    }

    public static String mapToJson(Map<String, String> presentation) {
        return new Gson().toJson(presentation);
    }

    private static void checkNoJsonNull(JsonObject object) throws JsonNullException {
        for (Map.Entry<String, JsonElement> objectsField: object.entrySet()) {
            if (objectsField.getValue().isJsonNull()) {
                throw new JsonNullException("Null value found for field: " + objectsField.getKey());
            }
        }
    }
}
