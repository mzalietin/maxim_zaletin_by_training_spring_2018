package by.training.tasks.cluster.node.reflection.serialization;

/**
 * Describes basic serializer functions.
 *
 * @param <P> presentation to which serialize
 */
public interface Serializer<P> {
    /**
     * Packages object to presentation.
     *
     * @param o object to serialize.
     * @return presentation of serialization.
     */
    P serialize(Object o);

    /**
     * Restores object from presentation.
     *
     * @param presentation presentation of serialization.
     * @param clazz expected object's class
     * @return deserialized object.
     */
    <T> T deserialize(P presentation, Class<T> clazz);
}
