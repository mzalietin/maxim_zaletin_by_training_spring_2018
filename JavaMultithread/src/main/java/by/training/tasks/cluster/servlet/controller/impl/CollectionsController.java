package by.training.tasks.cluster.servlet.controller.impl;

import by.training.tasks.cluster.node.reflection.proxy.handlers.CacheHandler;
import by.training.tasks.cluster.servlet.ClusterScope;
import by.training.tasks.cluster.node.INodeShard;
import by.training.tasks.cluster.node.dao.JsonDao;
import by.training.tasks.cluster.node.dao.NodeDao;
import by.training.tasks.cluster.node.dao.SqlProvider;
import by.training.tasks.cluster.node.exception.JsonNullException;
import by.training.tasks.cluster.node.exception.SchemaBuildException;
import by.training.tasks.cluster.node.schema.JsonSchema;
import by.training.tasks.cluster.servlet.controller.ClusterController;
import by.training.tasks.cluster.servlet.controller.ServerMessages;
import by.training.tasks.cluster.servlet.filter.ParametersFilter;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class CollectionsController extends ClusterController {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String collectionName = (String) req.getAttribute(ParametersFilter.COLLECTION_NAME_ATTRIBUTE);
        String requestSource = req.getHeader(ClusterScope.CLUSTER_REQUEST_HEADER);

        ClusterScope cluster = getClusterScope();

        String properAddress = cluster.getProperAddress(collectionName);

        if (properAddress == null) {
            resp.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, ServerMessages.NODE_IS_DOWN);
        } else {
            if (req.getRequestURL().toString().contains(properAddress)) {
                INodeShard node = getNode();

                if (collectionName.isEmpty()) {
                    resp.sendError(HttpServletResponse.SC_BAD_REQUEST, ServerMessages.COLLECTION_NAME_IS_EMPTY);
                }

                if (node.collectionExists(collectionName)) {
                    resp.sendError(HttpServletResponse.SC_BAD_REQUEST, ServerMessages.COLLECTION_EXISTS);
                } else {
                    try {
                        String body = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

                        if (node.createCollection(collectionName, buildCollection(body, collectionName))) {
                            resp.setStatus(HttpServletResponse.SC_CREATED);

                            if (cluster.isAnotherNodeOrExternalAddress(requestSource)) {
                                cluster.replicateOnPost(req.getRequestURI(), body);
                            }
                        } else {
                            resp.sendError(HttpServletResponse.SC_BAD_REQUEST,
                                    ServerMessages.NODE_CAPACITY_OVERFLOW);
                        }
                    } catch (JsonNullException | SchemaBuildException e) {
                        resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
                    }
                }
            } else {
                sendResponseWhenCollectionNotFound(req, resp, collectionName);
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String collectionName = (String) req.getAttribute(ParametersFilter.COLLECTION_NAME_ATTRIBUTE);
        String requestSource = req.getHeader(ClusterScope.CLUSTER_REQUEST_HEADER);

        INodeShard node = getNode();
        ClusterScope cluster = getClusterScope();

        boolean isExternalRequest = requestSource == null;

        String jsonResponse;

        if (collectionName.isEmpty()) {
            List<String> collectionsList = node.getCollectionsList();
            if (isExternalRequest) {
                List<String> otherShardsCollections = cluster.searchAllCollectionsOnGet(req.getRequestURI());
                collectionsList.addAll(otherShardsCollections);
            }
            jsonResponse = new Gson().toJson(collectionsList);
            sendJsonResponse(resp, jsonResponse);
        } else {
            if (node.collectionExists(collectionName)) {
                NodeDao collectionDao = node.getCollection(collectionName);
                jsonResponse = collectionDao.getDataSchema().toString();
                sendJsonResponse(resp, jsonResponse);
            } else {
                sendResponseWhenCollectionNotFound(req, resp, collectionName);
            }
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String collectionName = (String) req.getAttribute(ParametersFilter.COLLECTION_NAME_ATTRIBUTE);
        String requestSource = req.getHeader(ClusterScope.CLUSTER_REQUEST_HEADER);

        if (collectionName.isEmpty()) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, ServerMessages.COLLECTION_NAME_IS_EMPTY);
        }

        INodeShard node = getNode();
        ClusterScope cluster = getClusterScope();

        if (node.collectionExists(collectionName)) {
            try {
                String body = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

                node.updateCollection(collectionName, buildCollection(body, collectionName));

                if (cluster.isAnotherNodeOrExternalAddress(requestSource)) {
                    cluster.synchronizeOnUpdate(req.getRequestURI(), body);
                }
            } catch (JsonNullException | SchemaBuildException e) {
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
            }
        } else {
            sendResponseWhenCollectionNotFound(req, resp, collectionName);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String collectionName = (String) req.getAttribute(ParametersFilter.COLLECTION_NAME_ATTRIBUTE);
        String requestSource = req.getHeader(ClusterScope.CLUSTER_REQUEST_HEADER);

        if (collectionName.isEmpty()) {
            resp.sendError(HttpServletResponse.SC_CONFLICT, ServerMessages.COLLECTION_NAME_IS_EMPTY);
        }

        INodeShard node = getNode();
        ClusterScope cluster = getClusterScope();

        if (node.collectionExists(collectionName)) {
            node.removeCollection(collectionName);

            if (cluster.isAnotherNodeOrExternalAddress(requestSource)) {
                cluster.synchronizeOnDelete(req.getRequestURI());
            }
        } else {
            sendResponseWhenCollectionNotFound(req, resp, collectionName);
        }
    }

    private NodeDao buildCollection(String json, String name) throws IOException, SchemaBuildException {

        if (json.trim().isEmpty()) {
            throw new JsonNullException(ServerMessages.EMPTY_BODY);
        }

        JsonSchema schema = getAppContext().getBean(JsonSchema.class, json);

        SqlProvider sqlProvider = getAppContext().getBean(SqlProvider.class);
        sqlProvider.setTableName(name);

        JsonDao dao = getAppContext().getBean(JsonDao.class);
        dao.setDataSchema(schema);
        dao.setSqlProvider(sqlProvider);

        CacheHandler handler = getAppContext().getBean(CacheHandler.class);
        handler.setTarget(dao);

        final String DAO_PROXY_BEAN = "dao-proxy";
        return (NodeDao) getAppContext().getBean(DAO_PROXY_BEAN, JsonDao.class, handler);
    }
}
