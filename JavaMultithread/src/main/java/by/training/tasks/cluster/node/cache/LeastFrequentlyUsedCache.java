package by.training.tasks.cluster.node.cache;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class LeastFrequentlyUsedCache<K, V> implements Cache<K, V> {
    private final int capacity;

    private final HashMap<K, V> storage;
    private final HashMap<K, Long> frequencyKeys;
    private final TreeMap<Long, LinkedList<K>> frequencyBuckets;

    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock(true);
    private final Lock write = lock.writeLock();
    private final Lock read = lock.readLock();

    public LeastFrequentlyUsedCache(int maxSize) {
        if (maxSize > 0) {
            capacity = maxSize;
            storage = new HashMap<>(maxSize, 1.0f);
            frequencyKeys = new HashMap<>(maxSize, 1.0f);
            frequencyBuckets = new TreeMap<>();
        } else {
            throw new IllegalArgumentException("Cache size must be greater than zero");
        }
    }

    @Override
    public void insertIfAbsent(K key, V value) {
        Objects.requireNonNull(key);
        Objects.requireNonNull(value);

        write.lock();
        try {
            if (storage.putIfAbsent(key, value) == null) {
                if (storage.size() > capacity) {
                    //eviction
                    Map.Entry<Long, LinkedList<K>> lfuEntry = frequencyBuckets.firstEntry();
                    LinkedList<K> lfuList = lfuEntry.getValue();
                    K lfuKey = lfuList.remove();

                    if (lfuList.isEmpty()) {
                        frequencyBuckets.remove(lfuEntry.getKey());
                    }

                    frequencyKeys.remove(lfuKey);
                    storage.remove(lfuKey);
                }

                //frequency tracking
                final long zeroFreq = 0;
                frequencyKeys.put(key, zeroFreq);
                if (frequencyBuckets.containsKey(zeroFreq)) {
                    frequencyBuckets.get(zeroFreq).add(key);
                } else {
                    frequencyBuckets.put(zeroFreq, new LinkedList<>());
                }
            }
        } finally {
            write.unlock();
        }
    }

    @Override
    public V retrieve(K key) {
        read.lock();
        try {
            V value = storage.get(key);
            if (value != null) {
                promoteFrequency(key);
            }

            return value;
        } finally {
            read.unlock();
        }
    }

    @Override
    public void updateIfPresent(K key, V value) {
        Objects.requireNonNull(key);
        Objects.requireNonNull(value);

        write.lock();
        try {
            if (storage.containsKey(key)) {
                storage.put(key, value);
            }
        } finally {
            write.unlock();
        }
    }


    @Override
    public void delete(K key) {
        write.lock();
        try {
            if (storage.remove(key) != null) {
                Long freq = frequencyKeys.remove(key);
                List<K> keys = frequencyBuckets.get(freq);
                keys.remove(key);
                if (keys.isEmpty()) {
                    frequencyBuckets.remove(freq);
                }
            }
        } finally {
            write.lock();
        }
    }

    @Override
    public void flush() {
        write.lock();
        try {
            storage.clear();
            frequencyKeys.clear();
            frequencyBuckets.clear();
        } finally {
            write.unlock();
        }
    }

    private void promoteFrequency(K key) {
        Long freq = frequencyKeys.get(key);
        LinkedList<K> curFreqKeyList = frequencyBuckets.get(freq);
        curFreqKeyList.remove(key);
        if (curFreqKeyList.isEmpty()) {
            frequencyBuckets.remove(freq);
        }
        LinkedList<K> nextFreqKeyList = frequencyBuckets.get(freq + 1);
        if (nextFreqKeyList == null) {
            nextFreqKeyList = new LinkedList<>();
            nextFreqKeyList.add(key);
            frequencyBuckets.put(freq + 1, nextFreqKeyList);
        } else {
            nextFreqKeyList.add(key);
        }
        frequencyKeys.put(key, freq + 1);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        final String keyValueSeparator = " : ";
        sb.append("LFU").append(System.lineSeparator());
        for (Map.Entry<K, V> entry: storage.entrySet()) {
            sb.append(entry.getKey()).append(keyValueSeparator).append(entry.getValue());
            sb.append(keyValueSeparator).append(frequencyKeys.get(entry.getKey())).append(System.lineSeparator());
        }
        return sb.toString();
    }
}
