package by.training.tasks.cluster.node.reflection.proxy;

import by.training.tasks.cluster.node.reflection.annotation.Proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;

/**
 * Factory to obtain instances of classes that marked with custom <code>@Proxy</code> annotation.
 * If annotation is not present, creates simple class instance via default constructor.
 */
public final class ProxyFactory {
    private ProxyFactory() { }

    /**
     * Creates proxy instance with specified invocation handler.
     * @param clazz proxied class
     * @param handler invocation handler for class methods
     * @return proxy instance
     */
    public static Object getInstanceOf(Class clazz, InvocationHandler handler) {
        try {
            if (clazz.isAnnotationPresent(Proxy.class)) {
                return java.lang.reflect.Proxy.newProxyInstance(clazz.getClassLoader(),
                        clazz.getInterfaces(), handler);
            } else {
                return clazz.newInstance();
            }
        } catch (InstantiationException | IllegalAccessException e) {
            throw new IllegalArgumentException("Failed to create class instance", e);
        }
    }

    /**
     * Creates proxy instance with handler specified as annotation parameter <code>invocationHandler</code>.
     * This handler ought to have constructor with single <code>Object</code> arg to provide proxy target.
     * @param clazz proxied class
     * @return proxy instance
     */
    public static Object getInstanceOf(Class clazz) {
        try {
            if (clazz.isAnnotationPresent(Proxy.class)) {
                return java.lang.reflect.Proxy.newProxyInstance(clazz.getClassLoader(),
                        clazz.getInterfaces(),
                        (InvocationHandler) ((Proxy) clazz
								.getAnnotation(Proxy.class))
                                .invocationHandler()
                                .getConstructor(Object.class)
                                .newInstance(clazz.newInstance()));
            } else {
                return clazz.newInstance();
            }
        } catch (InstantiationException
                | IllegalAccessException
                | NoSuchMethodException
                | InvocationTargetException e) {
            throw new IllegalArgumentException("Failed to create class instance", e);
        }
    }
}
