package by.training.tasks.cluster.servlet;

import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class HealthCheckService {
    private final Map<String, ShardStatus> healthStatus;

    private final CloseableHttpClient httpClient = HttpClients.createDefault();

    HealthCheckService(Map<String, ShardStatus> healthStatusStart) {
        healthStatus = healthStatusStart;
    }

    void setStatus(String host, ShardStatus status) {
        healthStatus.put(host, status);
    }

    ShardStatus getLastStatus(String host) {
        return healthStatus.get(host);
    }

    ShardStatus refreshStatus(String host) {
        ShardStatus status;

        HttpGet get = new HttpGet(host);

        try {
            try (CloseableHttpResponse response = httpClient.execute(get)) {
                if (response.getStatusLine().getStatusCode() >= HttpStatus.SC_INTERNAL_SERVER_ERROR) {
                    status = ShardStatus.RED;
                } else {
                    status = ShardStatus.GREEN;
                }
            }
        } catch (IOException e) {
            status = ShardStatus.RED;
        }

        healthStatus.put(host, status);

        return status;
    }

    public Map<String, ShardStatus> getClusterStatus() {
        healthStatus.forEach((host, status) -> refreshStatus(host));

        return new HashMap<>(healthStatus);
    }
}
