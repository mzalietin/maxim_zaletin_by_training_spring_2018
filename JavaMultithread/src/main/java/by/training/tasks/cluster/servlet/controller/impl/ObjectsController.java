package by.training.tasks.cluster.servlet.controller.impl;

import by.training.tasks.cluster.node.INodeShard;
import by.training.tasks.cluster.node.entity.JsonEntity;
import by.training.tasks.cluster.node.entity.NodeEntity;
import by.training.tasks.cluster.node.exception.JsonNullException;
import by.training.tasks.cluster.node.exception.SchemaValidationException;
import by.training.tasks.cluster.servlet.ClusterScope;
import by.training.tasks.cluster.servlet.controller.ClusterController;
import by.training.tasks.cluster.servlet.controller.ServerMessages;
import by.training.tasks.cluster.servlet.filter.ParametersFilter;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ObjectsController extends ClusterController {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String collectionName = (String) req.getAttribute(ParametersFilter.COLLECTION_NAME_ATTRIBUTE);
        String requestSource = req.getHeader(ClusterScope.CLUSTER_REQUEST_HEADER);

        INodeShard node = getNode();
        ClusterScope cluster = getClusterScope();

        if (node.collectionExists(collectionName)) {
            try {
                String body = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

                //validate and save on this shard
                int id = node.getCollection(collectionName).create(buildEntity(body));

                //commit log

                //replicate
                if (cluster.isAnotherNodeOrExternalAddress(requestSource)) {
                    cluster.replicateOnPost(req.getRequestURI(), body);
                }

                Map<String, String> entry = new HashMap<>();
                entry.put(ParametersFilter.ENTITY_ID_ATTRIBUTE, String.valueOf(id));
                String json = new Gson().toJson(entry);

                resp.setStatus(HttpServletResponse.SC_CREATED);

                sendJsonResponse(resp, json);
            } catch (SchemaValidationException | JsonNullException e) {
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
            }
        } else {
            sendResponseWhenCollectionNotFound(req, resp, collectionName);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String entityId = (String) req.getAttribute(ParametersFilter.ENTITY_ID_ATTRIBUTE);
        String collectionName = (String) req.getAttribute(ParametersFilter.COLLECTION_NAME_ATTRIBUTE);
        String requestSource = req.getHeader(ClusterScope.CLUSTER_REQUEST_HEADER);

        INodeShard node = getNode();
        ClusterScope cluster = getClusterScope();

        if (node.collectionExists(collectionName)) {
            Gson gson = new Gson();

            if (entityId.isEmpty()) {
                List entityList = node.getCollection(collectionName).getAll();
                sendJsonResponse(resp, gson.toJson(entityList));
            } else {
                try {
                    int id = Integer.valueOf(entityId);
                    String result = null;

                    Object cachedEntity = node.getCollection(collectionName).getOne(id, true);
                    if (cachedEntity != null) {
                        result = cachedEntity.toString();
                    } else {
                        if (cluster.isAnotherNodeOrExternalAddress(requestSource)) {
                            result = cluster.searchObjectOnGet(req.getRequestURI());
                            if (result == null) {
                                Object requestedEntity = node.getCollection(collectionName).getOne(id, false);
                                if (requestedEntity != null) {
                                    result = requestedEntity.toString();
                                }
                            }
                        }
                    }
                    if (result == null) {
                        resp.sendError(HttpServletResponse.SC_NOT_FOUND, ServerMessages.ENTITY_NOT_FOUND);
                    } else {
                        sendJsonResponse(resp, result);
                    }
                } catch (NumberFormatException e) {
                    resp.sendError(HttpServletResponse.SC_BAD_REQUEST, ServerMessages.INVALID_ID_FORMAT);
                }
            }
        } else {
            sendResponseWhenCollectionNotFound(req, resp, collectionName);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String collectionName = (String) req.getAttribute(ParametersFilter.COLLECTION_NAME_ATTRIBUTE);
        String entityId = (String) req.getAttribute(ParametersFilter.ENTITY_ID_ATTRIBUTE);
        String requestSource = req.getHeader(ClusterScope.CLUSTER_REQUEST_HEADER);

        INodeShard node = getNode();
        ClusterScope cluster = getClusterScope();

        if (node.collectionExists(collectionName)) {
            try {
                String body = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

                int id = Integer.valueOf(entityId);

                NodeEntity entity = buildEntity(body);
                entity.setId(id);

                node.getCollection(collectionName).update(entity);

                if (cluster.isAnotherNodeOrExternalAddress(requestSource)) {
                    cluster.synchronizeOnUpdate(req.getRequestURI(), body);
                }
            } catch (NumberFormatException e) {
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST, ServerMessages.INVALID_ID_FORMAT);
            } catch (SchemaValidationException | JsonNullException e) {
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
            }
        } else {
            sendResponseWhenCollectionNotFound(req, resp, collectionName);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String collectionName = (String) req.getAttribute(ParametersFilter.COLLECTION_NAME_ATTRIBUTE);
        String entityId = (String) req.getAttribute(ParametersFilter.ENTITY_ID_ATTRIBUTE);
        String requestSource = req.getHeader(ClusterScope.CLUSTER_REQUEST_HEADER);

        INodeShard node = getNode();
        ClusterScope cluster = getClusterScope();

        if (node.collectionExists(collectionName)) {
            try {
                int id = Integer.valueOf(entityId);

                node.getCollection(collectionName).delete(id);

                if (cluster.isAnotherNodeOrExternalAddress(requestSource)) {
                    cluster.synchronizeOnDelete(req.getRequestURI());
                }
            } catch (NumberFormatException e) {
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST, ServerMessages.INVALID_ID_FORMAT);
            }
        } else {
            sendResponseWhenCollectionNotFound(req, resp, collectionName);
        }
    }

    private NodeEntity buildEntity(String json) throws IOException {
        if (json.trim().isEmpty()) {
            throw new JsonNullException(ServerMessages.EMPTY_BODY);
        }

        return new JsonEntity(json);
    }
}
