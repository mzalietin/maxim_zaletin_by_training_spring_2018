package by.training.tasks.cluster.node.reflection.proxy.handlers;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class TimingHandler implements InvocationHandler {
    private final Object target;

    public TimingHandler(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        long start = System.nanoTime();
        Object result = method.invoke(target, args);
        long elapsed = System.nanoTime() - start;
        org.apache.logging.log4j.LogManager.getLogger(TimingHandler.class).info("Executing {} finished in {} ns", method.getName(), elapsed);

        return result;
    }
}
