package by.training.tasks.cluster.servlet.controller;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.http.HttpServlet;

public abstract class WebApplicationController extends HttpServlet {
    protected WebApplicationContext getAppContext() {
        return WebApplicationContextUtils.getRequiredWebApplicationContext(this.getServletContext());
    }
}
