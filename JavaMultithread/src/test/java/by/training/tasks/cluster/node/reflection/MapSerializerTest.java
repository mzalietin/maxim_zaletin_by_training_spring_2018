package by.training.tasks.cluster.node.reflection;

import by.training.tasks.cluster.node.reflection.serialization.MapSerializer;
import org.junit.Rule;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.rules.ExpectedException;
import by.training.tasks.cluster.node.reflection.entity.SomethingSerializable;
import by.training.tasks.cluster.node.reflection.entity.Student;

import java.util.HashMap;
import java.util.Map;

public class MapSerializerTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void notSerializableTest() {
        MapSerializer serializer = new MapSerializer();

        thrown.expect(IllegalArgumentException.class);

        serializer.serialize(new Student());
    }

    @Test
    public void checkCollection() throws NoSuchFieldException {
        MapSerializer serializer = new MapSerializer();

        SomethingSerializable inputObject = new SomethingSerializable();

        final int TEST_VALUE = 5;
        inputObject.setIntVal(TEST_VALUE);
        inputObject.setStrVal("Hello");
        inputObject.setNotPrimitiveVal(new Exception());

        Map<String, String> store = serializer.serialize(inputObject);

        final int SERIALIZABLE_FIELDS_COUNT = 4;
        assertEquals(SERIALIZABLE_FIELDS_COUNT, store.size());

        SomethingSerializable outputObject = serializer.deserialize(store, SomethingSerializable.class);

        assertEquals(TEST_VALUE, outputObject.getIntVal());

        assertNull(outputObject.getNotPrimitiveVal());
    }

    @Test
    public void deserializeTest() {
        MapSerializer serializer = new MapSerializer();

        Map<String, String> invalidStore = new HashMap<>();

        thrown.expect(IllegalArgumentException.class);

        serializer.deserialize(invalidStore, Object.class);

        invalidStore.put("some", "slacks");

        thrown.expect(IllegalStateException.class);

        serializer.deserialize(invalidStore, Object.class);
    }
}
