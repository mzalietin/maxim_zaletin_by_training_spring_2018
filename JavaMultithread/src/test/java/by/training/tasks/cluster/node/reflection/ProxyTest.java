package by.training.tasks.cluster.node.reflection;

import by.training.tasks.cluster.node.reflection.proxy.ProxyFactory;

import org.junit.Assert;
import org.junit.Test;

import by.training.tasks.cluster.node.reflection.entity.Person;
import by.training.tasks.cluster.node.reflection.entity.SomethingSerializable;
import by.training.tasks.cluster.node.reflection.entity.Student;

public class ProxyTest {

    @Test
    public void proxyInstanceTest() throws Throwable {
        Person student = (Person) ProxyFactory.getInstanceOf(Student.class);

        Assert.assertTrue(student.getClass().getSimpleName().contains("$Proxy"));

        SomethingSerializable smth = (SomethingSerializable) ProxyFactory.getInstanceOf(SomethingSerializable.class);

        Assert.assertFalse(smth.getClass().getSimpleName().contains("$Proxy"));
    }
}
