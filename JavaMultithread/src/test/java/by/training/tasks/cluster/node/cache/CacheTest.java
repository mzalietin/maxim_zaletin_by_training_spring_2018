package by.training.tasks.cluster.node.cache;

import static org.junit.Assert.*;

import org.junit.Test;

public class CacheTest {

    @Test(expected = NullPointerException.class)
    public void testLRU() {
        Cache<String, Integer> lruCache = new LeastRecentlyUsedCache<>(4);

        testInsert(lruCache);
        testRetrieve(lruCache);
        testUpdate(lruCache);
        testDelete(lruCache);
        testNonNullStorage(lruCache);

        //expected content after logic test
        final String CACHE_NAME = "LRU", LN = System.lineSeparator(), SEP = " : ";
        String expected = CACHE_NAME + LN +
                "one" + SEP + 1 + LN +
                "five" + SEP + 5 + LN +
                "three" + SEP + 33 + LN +
                "six" + SEP + 6 + LN;

        String result = testLogic(lruCache);

        assertEquals(expected, result);
    }

    @Test(expected = NullPointerException.class)
    public void testLFU() {
        Cache<String, Integer> lfuCache = new LeastFrequentlyUsedCache<>(4);

        testInsert(lfuCache);
        testRetrieve(lfuCache);
        testUpdate(lfuCache);
        testDelete(lfuCache);
        testNonNullStorage(lfuCache);

        //expected content after logic test
        final String CACHE_NAME = "LFU", LN = System.lineSeparator(), SEP = " : ";
        String expected = CACHE_NAME + LN +
                "six" + SEP + 6 + SEP + 0 + LN +
                "three" + SEP + 33 + SEP + 2 + LN +
                "five" + SEP + 5 + SEP + 0 + LN +
                "one" + SEP + 1 + SEP + 1 + LN;

        String result = testLogic(lfuCache);

        assertEquals(expected, result);
    }

    private void testInsert(Cache<String, Integer> testCache) {
        String testKey = "fifty five";
        Integer testValue = 55;
        Integer otherValue = 60;

        testCache.insertIfAbsent(testKey, testValue);
        assertSame(testCache.retrieve(testKey), testValue);
        testCache.insertIfAbsent(testKey, otherValue);
        assertSame(testCache.retrieve(testKey), testValue);

        testCache.flush();
    }

    private void testRetrieve(Cache<String, Integer> testCache) {
        String testKey = "sixty two";
        String nonExistKey = "thirty eight";
        Integer testValue = 62;

        testCache.insertIfAbsent(testKey, testValue);

        assertNotNull(testCache.retrieve(testKey));
        assertNull(testCache.retrieve(nonExistKey));

        testCache.flush();
    }

    private void testUpdate(Cache<String, Integer> testCache) {
        String testKey = "forty four";
        Integer testValue = 44;
        Integer anotherValue = 444;

        testCache.insertIfAbsent(testKey, testValue);
        assertNotNull(testCache.retrieve(testKey));
        testCache.updateIfPresent(testKey, anotherValue);
        assertNotSame(testCache.retrieve(testKey), testValue);
        assertSame(testCache.retrieve(testKey), anotherValue);

        testCache.flush();
    }

    private void testDelete(Cache<String, Integer> testCache) {
        String testKey = "sixty four";
        Integer testValue = 64;

        testCache.insertIfAbsent(testKey, testValue);
        assertNotNull(testCache.retrieve(testKey));
        testCache.delete(testKey);
        assertNull(testCache.retrieve(testKey));
    }

    private void testNonNullStorage(Cache<String, Integer> testCache) {
        final String testKey = "hundred";
        final Integer testValue = 100;

        //should throw NPE
        testCache.insertIfAbsent(null, testValue);
        testCache.insertIfAbsent(testKey, null);

        testCache.updateIfPresent(null, testValue);
        testCache.updateIfPresent(testKey, null);

        testCache.flush();
    }

    private String testLogic(Cache<String, Integer> testCache) {
        try {
            testCache.insertIfAbsent("one", 1);
            testCache.insertIfAbsent("two", 2);
            testCache.insertIfAbsent("three", 3);
            testCache.insertIfAbsent("four", 4);
            testCache.retrieve("one");
            testCache.retrieve("three");
            testCache.insertIfAbsent("five", 5);
            testCache.insertIfAbsent("three", 33);
            testCache.insertIfAbsent("six", 6);

            return testCache.toString();
        } finally {
            testCache.flush();
        }
    }
}
