package by.training.tasks.cluster.node.reflection.entity;

public interface Person {
    void setName(String name);
    void work();
    void eat();
    void sleep();
}
