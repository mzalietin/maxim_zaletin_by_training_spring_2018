package by.training.tasks.cluster.node.dao;

import by.training.tasks.cluster.node.entity.JsonEntity;
import by.training.tasks.cluster.node.entity.NodeEntity;
import static org.junit.Assert.*;

import by.training.tasks.cluster.node.schema.JsonSchema;
import org.junit.*;
import org.junit.runners.MethodSorters;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import static org.mockito.Mockito.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NodeDaoTest {
    private static JsonDao dao;

    private static JsonEntity entity;
    private static JsonEntity anotherEntity;
    private static JsonSchema schema;
    private static JdbcTemplate jdbcTemplateObject;
    private static SqlProvider sqlProvider;

    private static final String COLLECTION_NAME = "collection_name";
    private static final String JSON_DATA_1 = "{\"five\": 5}";
    private static final String JSON_DATA_2 = "{\"six\": 6}";
    private static final String RES_PATH = "sql/queries";

    @BeforeClass
    public static void setUp() throws Exception {
        entity = spy(new JsonEntity(JSON_DATA_1));
        anotherEntity = spy(new JsonEntity(JSON_DATA_2));
        sqlProvider = spy(new SqlProvider(RES_PATH));
        sqlProvider.setTableName(COLLECTION_NAME);

        DriverManagerDataSource testDs = new DriverManagerDataSource();
        testDs.setDriverClassName(TestDB.DRIVER);
        testDs.setUrl(TestDB.URL);
        testDs.setUsername(TestDB.USERNAME);
        testDs.setPassword(TestDB.PASSWORD);

        jdbcTemplateObject = new JdbcTemplate(testDs);

        schema = mock(JsonSchema.class);

        dao = new JsonDao();

        dao.setSqlProvider(sqlProvider);
        dao.setJdbcTemplate(jdbcTemplateObject);
        dao.setDataSchema(schema);
        dao.init();
    }

    @Test
    public void test_A_createsAndValidatesEntityAndReturnsId() throws Exception {
        int id = dao.create(entity);

        verify(schema).validate(entity);
        verify(entity).setId(id);
    }

    @Test
    public void test_B_returnsCreatedObjectById() throws Exception {
        int id = entity.getId();

        NodeEntity result = dao.getOne(id, false);

        assertEquals(entity.toString(), result.toString());
    }

    @Test
    public void test_C_updatesAndValidates() throws Exception {
        anotherEntity.setId(entity.getId());

        dao.update(anotherEntity);

        verify(schema).validate(entity);

        NodeEntity result = dao.getOne(anotherEntity.getId(), false);

        assertEquals(anotherEntity.toString(), result.toString());
    }

    @Test
    public void test_D_deletes() throws Exception {
        int id = anotherEntity.getId();

        dao.delete(id);

        assertNull(dao.getOne(id, false));
    }

    @AfterClass
    public static void tearDown() throws Exception {
        dao.shutDown();
    }
}
