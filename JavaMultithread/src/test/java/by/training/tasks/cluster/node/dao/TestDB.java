package by.training.tasks.cluster.node.dao;

public interface TestDB {
    String DRIVER = "org.postgresql.Driver";
    String URL = "jdbc:postgresql://localhost:5432/local-tests";
    String USERNAME = "java";
    String PASSWORD = "junit412";
}
