package by.training.tasks.cluster.node.reflection.entity;

import by.training.tasks.cluster.node.reflection.annotation.Proxy;
import by.training.tasks.cluster.node.reflection.proxy.handlers.TimingHandler;

@Proxy(invocationHandler = TimingHandler.class)
public class Student implements Person {
    private String name;

    public Student() {}

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void work() {
        System.out.println(name + " goes to university");
    }

    @Override
    public void eat() {
        System.out.println(name + " eats fast-food");
    }

    @Override
    public void sleep() {
        System.out.println(name + " sleeps 5 hours");
    }
}
