package by.training.tasks.cluster.node.entity.impl;

import by.training.tasks.cluster.node.reflection.annotation.Serializable;

@Serializable
public class Dog {
    @Serializable
    private String name;

    @Serializable
    private int age;

    @Serializable
    private String breed;

    public Dog() { }

    public Dog(String name, int age, String breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getBreed() {
        return breed;
    }
}
