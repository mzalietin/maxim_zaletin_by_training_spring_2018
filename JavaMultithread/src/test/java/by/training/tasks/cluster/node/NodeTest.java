package by.training.tasks.cluster.node;

import by.training.tasks.cluster.node.dao.JsonDao;
import by.training.tasks.cluster.node.dao.NodeDao;
import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.mockito.Mockito.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NodeTest {

    private static INodeShard node;
    private static JsonDao daoSampleOne;
    private static JsonDao daoSampleTwo;

    private final String DAO_NAME = "sample";
    @BeforeClass
    @SuppressWarnings("unchecked")
    public static void setUp() {
        final int CAP = 1;
        node = new NodeShard(CAP);
        daoSampleOne = mock(JsonDao.class);
        daoSampleTwo = mock(JsonDao.class);
    }

    @Test
    public void test_A_createdAndExists() {
        node.createCollection(DAO_NAME, daoSampleOne);

        assertTrue(node.collectionExists(DAO_NAME));
        assertTrue(node.getCollectionsList().contains(DAO_NAME));
        assertEquals(node.getCollectionsList().size(), 1);

        //out of capacity
        assertFalse(node.createCollection(DAO_NAME, daoSampleTwo));
    }

    @Test
    public void test_B_read() {
        NodeDao someDao = node.getCollection(DAO_NAME);

        assertSame(someDao, daoSampleOne);
    }

    @Test
    public void test_C_update() {
        node.updateCollection(DAO_NAME, daoSampleTwo);

        assertNotSame(node.getCollection(DAO_NAME), daoSampleOne);
        assertEquals(node.getCollectionsList().size(), 1);
    }

    @Test
    public void test_D_delete() {
        node.removeCollection(DAO_NAME);

        assertFalse(node.collectionExists(DAO_NAME));
        assertFalse(node.getCollectionsList().contains(DAO_NAME));
        assertEquals(node.getCollectionsList().size(), 0);
    }

    @Test(expected = NullPointerException.class)
    public void test_E_acceptsNotNullOnly() {
        node.createCollection(DAO_NAME, null);
        node.createCollection(null, daoSampleOne);
        node.updateCollection(DAO_NAME, null);
        node.updateCollection(null, daoSampleOne);
        node.getCollection(null);
        node.removeCollection(null);
    }
}
