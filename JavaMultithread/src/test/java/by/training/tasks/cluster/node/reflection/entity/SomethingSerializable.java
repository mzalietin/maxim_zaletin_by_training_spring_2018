package by.training.tasks.cluster.node.reflection.entity;

import by.training.tasks.cluster.node.reflection.annotation.Serializable;

@Serializable
public class SomethingSerializable {

    @Serializable
    private int intVal;

    @Serializable
    private long longVal;

    @Serializable
    protected double doubleVal;

    @Serializable
    public String strVal;

    @Serializable
    Exception notPrimitiveVal;

    public void setIntVal(int intVal) {
        this.intVal = intVal;
    }

    public void setLongVal(long longVal) {
        this.longVal = longVal;
    }

    public void setDoubleVal(double doubleVal) {
        this.doubleVal = doubleVal;
    }

    public void setStrVal(String strVal) {
        this.strVal = strVal;
    }

    public void setNotPrimitiveVal(Exception notPrimitiveVal) {
        this.notPrimitiveVal = notPrimitiveVal;
    }

    public int getIntVal() {
        return intVal;
    }

    public long getLongVal() {
        return longVal;
    }

    public double getDoubleVal() {
        return doubleVal;
    }

    public String getStrVal() {
        return strVal;
    }

    public Exception getNotPrimitiveVal() {
        return notPrimitiveVal;
    }
}
