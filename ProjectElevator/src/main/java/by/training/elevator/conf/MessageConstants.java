package by.training.elevator.conf;

public interface MessageConstants {
    String STARTING_TRANSPORTATION = "Transportation process started";
    String COMPLETION_TRANSPORTATION = "Transportation process finished";
    String MOVING_ELEVATOR = "Moving elevator from story {} to story {}";
    String BOARDING_OF_PASSENGER = "Passenger #{} picked up at story {}";
    String DEBOARDING_OF_PASSENGER = "Passenger #{} arrived at story {}";
}
